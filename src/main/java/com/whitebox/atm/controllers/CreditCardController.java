package com.whitebox.atm.controllers;

import com.whitebox.atm.models.Account;
import com.whitebox.atm.models.CreditCard;
import com.whitebox.atm.services.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
public class CreditCardController {
    @Autowired
    private CreditCardService creditCardService;

    @GetMapping("/creditCards")
    public ResponseEntity<List<CreditCard>> getCreditCards() {
        return ResponseEntity.ok(creditCardService.getCreditCards());
    }

    @PostMapping(value = "/creditCards")
    public ResponseEntity<CreditCard> addCreditCard(@Valid @RequestBody CreditCard creditCard) {
        return ResponseEntity.ok(creditCardService.addCreditCard(creditCard));
    }

    @GetMapping("/creditCards/{id}")
    public ResponseEntity<CreditCard> getCreditCard(@PathVariable @Positive int id) {
        return ResponseEntity.ok(creditCardService.getCreditCard(id));
    }

    @GetMapping("/creditCards/{id}/account")
    public ResponseEntity<Account> getAccountByCreditCardId(@PathVariable("id") @Positive int creditCardId) {
        return ResponseEntity.ok(creditCardService.getAccountByCreditCardId(creditCardId));
    }
}
