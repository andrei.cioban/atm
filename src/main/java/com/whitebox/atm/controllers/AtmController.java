package com.whitebox.atm.controllers;

import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteInventory;
import com.whitebox.atm.models.CashAmount;
import com.whitebox.atm.services.AtmService;
import com.whitebox.atm.services.BanknoteInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
public class AtmController {
    @Autowired
    private AtmService atmService;

    @Autowired
    private BanknoteInventoryService banknoteInventoryService;

    @GetMapping("/atm/funds")
    public ResponseEntity<List<BanknoteInventory>> getFunds() {
        return ResponseEntity.ok(banknoteInventoryService.getBanknoteInventories());
    }

    @PostMapping("/atm/funds")
    public ResponseEntity<List<BanknoteInventory>> addFunds(@Valid @RequestBody List<BanknoteInventory> banknoteInventories) {
        return ResponseEntity.ok(banknoteInventoryService.addBanknoteInventories(banknoteInventories));
    }

    @PutMapping("/atm/funds")
    public ResponseEntity<List<BanknoteInventory>> modifyFunds(@Valid @RequestBody List<BanknoteInventory> banknoteInventories) {
        return ResponseEntity.ok(banknoteInventoryService.getModifiedBanknoteInventories(banknoteInventories));
    }

    @GetMapping("/atm/funds/balance")
    public ResponseEntity<Integer> getAtmBalance() {
        return ResponseEntity.ok(banknoteInventoryService.getAmount());
    }

    @GetMapping("/atm/funds/banknotes")
    public ResponseEntity<List<Banknote>> getBanknotes() {
        return ResponseEntity.ok(banknoteInventoryService.getBanknotes());
    }

    @GetMapping("/atm/balance")
    public ResponseEntity<Integer> getBalance(@RequestParam @Positive int creditCardId) {
        return ResponseEntity.ok(atmService.getBalance(creditCardId));
    }

    @GetMapping("/atm/withdraw")
    public ResponseEntity<List<CashAmount>> showWithdrawal(@RequestParam @Positive int amount, @RequestParam @Positive int creditCardId) {
        return ResponseEntity.ok(atmService.withdraw(amount, creditCardId));
    }

    @PostMapping("/atm/deposit")
    public ResponseEntity<Integer> depositMoney(@Valid @RequestBody List<CashAmount> cashAmounts, @RequestParam @Positive int creditCardId) {
        return ResponseEntity.ok(atmService.deposit(cashAmounts, creditCardId));
    }
}
