package com.whitebox.atm.controllers;

import com.whitebox.atm.models.Account;
import com.whitebox.atm.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts")
    public ResponseEntity<List<Account>> getAccounts() {
        return ResponseEntity.ok(accountService.getAccounts());
    }

    @PostMapping("/accounts")
    public ResponseEntity<Account> addAccount(@Valid @RequestBody Account account) {
        return ResponseEntity.ok(accountService.addAccount(account));
    }

    @GetMapping("/accounts/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable @Positive int id) {
        return ResponseEntity.ok(accountService.getAccount(id));
    }
}
