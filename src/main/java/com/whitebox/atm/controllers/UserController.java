package com.whitebox.atm.controllers;

import com.whitebox.atm.models.User;
import com.whitebox.atm.models.CreditCard;
import com.whitebox.atm.models.UserRole;
import com.whitebox.atm.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping(value = "/users")
    public ResponseEntity<User> addUser(@Valid @RequestBody User user) {
        return ResponseEntity.ok(userService.addUser(user));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable @Positive int id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @GetMapping("/user")
    public ResponseEntity<User> getUserByCredentials(@RequestParam String username, @RequestParam String password,
                                                         @RequestParam("role") UserRole userRole) {
        return ResponseEntity.ok(userService.getUser(username, password, userRole));
    }

    @GetMapping("/users/{userId}/creditCards")
    public ResponseEntity<List<CreditCard>> getCreditCardsByUserId(@PathVariable @Positive int userId) {
        return ResponseEntity.ok(userService.getCreditCardsByUserId(userId));
    }

    @GetMapping("/users/roles")
    public ResponseEntity<List<User>> getUsersByRole(@RequestParam("role") @Valid UserRole userRole) {
        return ResponseEntity.ok(userService.getUsersByRole(userRole));
    }
}
