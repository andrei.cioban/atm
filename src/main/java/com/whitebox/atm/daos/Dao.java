package com.whitebox.atm.daos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public abstract class Dao<T, V> {
    @Autowired
    public EntityManager entityManager;

    public abstract List<T> findAll();
    public abstract T findById(V id);
    public abstract void save(T t);
}
