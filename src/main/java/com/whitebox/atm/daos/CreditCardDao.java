package com.whitebox.atm.daos;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.DataNotFoundException;
import com.whitebox.atm.models.Account;
import com.whitebox.atm.models.CreditCard;
import com.whitebox.atm.models.QCreditCard;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CreditCardDao extends Dao<CreditCard, Integer> {
    private final QCreditCard qCreditCard = QCreditCard.creditCard;

    @Override
    public List<CreditCard> findAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qCreditCard).fetch();
    }

    @Override
    public CreditCard findById(Integer id) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        CreditCard creditCard = queryFactory.selectFrom(qCreditCard).where(qCreditCard.id.eq(id)).fetchFirst();

        if (creditCard == null) {
            throw new DataNotFoundException("Credit card not found.");
        }
        return creditCard;
    }

    @Override
    public void save(CreditCard creditCard) {
        entityManager.persist(creditCard);
    }

    public Account findAccount(Integer creditCardId) {
        return findById(creditCardId).getAccount();
    }
}
