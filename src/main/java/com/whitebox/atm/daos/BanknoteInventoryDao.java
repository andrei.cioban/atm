package com.whitebox.atm.daos;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.DataNotFoundException;
import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteInventory;
import com.whitebox.atm.models.QBanknoteInventory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BanknoteInventoryDao extends Dao<BanknoteInventory, Banknote> {
    private final QBanknoteInventory qBanknoteInventory = QBanknoteInventory.banknoteInventory;

    @Override
    public List<BanknoteInventory> findAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qBanknoteInventory).fetch();
    }

    @Override
    public BanknoteInventory findById(Banknote banknote) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        BanknoteInventory banknoteInventory = queryFactory.selectFrom(qBanknoteInventory)
                .where(qBanknoteInventory.banknote.eq(banknote)).fetchFirst();

        if (banknoteInventory == null) {
            throw new DataNotFoundException("Banknote not found");
        }
        return banknoteInventory;
    }

    @Override
    public void save(BanknoteInventory banknoteInventory) {
        entityManager.persist(banknoteInventory);
    }

    public void updateQuantity(Banknote banknote, int quantity) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        queryFactory.update(qBanknoteInventory).where(qBanknoteInventory.banknote.eq(banknote))
                .set(qBanknoteInventory.quantity, quantity)
                .execute();
    }

    public List<Banknote> findBanknotes() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory
                .selectFrom(qBanknoteInventory)
                .fetch()
                .stream().map(BanknoteInventory::getBanknote)
                .toList();
    }
}
