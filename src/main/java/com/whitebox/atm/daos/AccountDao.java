package com.whitebox.atm.daos;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.DataNotFoundException;
import com.whitebox.atm.models.Account;
import com.whitebox.atm.models.CreditCard;
import com.whitebox.atm.models.QAccount;
import com.whitebox.atm.models.QCreditCard;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDao extends Dao<Account, Integer> {
    private final QAccount qAccount = QAccount.account;

    @Override
    public List<Account> findAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qAccount).fetch();
    }

    @Override
    public Account findById(Integer id) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        Account account = queryFactory.selectFrom(qAccount).where(qAccount.id.eq(id)).fetchFirst();

        if (account == null) {
            throw new DataNotFoundException("Account not found.");
        }
        return account;
    }

    @Override
    public void save(Account account) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        QCreditCard qCreditCard = QCreditCard.creditCard;

        CreditCard creditCard = queryFactory
                .selectFrom(qCreditCard)
                .where(qCreditCard.id.eq(account.getCreditCard().getId()))
                .fetchFirst();

        creditCard.setAccount(account);
        account.setCreditCard(creditCard);
        entityManager.persist(account);
    }

    public void updateBalance(Integer id, Integer newBalance) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        queryFactory.update(qAccount).where(qAccount.id.eq(id))
                .set(qAccount.balance, newBalance)
                .execute();
    }
}
