package com.whitebox.atm.daos;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.DataNotFoundException;
import com.whitebox.atm.models.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDao extends Dao<User, Integer> {
    private final QUser quser = QUser.user;

    @Override
    public List<User> findAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(quser).fetch();
    }

    @Override
    public User findById(Integer userId) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        User user = queryFactory.selectFrom(quser).where(quser.id.eq(userId)).fetchFirst();

        if (user == null) {
            throw new DataNotFoundException("User not found.");
        }
        return user;
    }

    @Override
    public void save(User user) {
        entityManager.persist(user);
    }

    public User findByCredentials(String username, String password, UserRole userRole) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        User user = queryFactory
                .selectFrom(quser)
                .where(quser.username.eq(username), quser.password.eq(password), quser.role.eq(userRole))
                .fetchFirst();

        if (user == null) {
            throw new DataNotFoundException("User not found.");
        }
        return user;
    }

    public List<CreditCard> findCreditCards(Integer userId) {
        return findById(userId).getCreditCards();
    }

    public List<User> findByRole(UserRole userRole) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        List<User> users = queryFactory
                .selectFrom(quser)
                .where(quser.role.eq(userRole))
                .fetch();

        if (users.isEmpty()) {
            throw new DataNotFoundException("No users for this role.");
        }

        return users;
    }
}
