package com.whitebox.atm.exceptions;

public class InvalidDataException extends RuntimeException {
    public InvalidDataException() {
        super("Introduced data invalid");
    }

    public InvalidDataException(String errorMessage) {
        super(errorMessage);
    }
}

