package com.whitebox.atm.models;

public enum Banknote {
    ONE_RON("1RON", 1),
    FIVE_RON("5RON", 5),
    TEN_RON("10RON", 10),
    TWENTY_RON("20RON", 20),
    FIFTY_RON("50RON", 50),
    ONE_HUNDRED_RON("100RON", 100);

    Banknote(String name, int value) {
        this.name = name;
        this.value = value;
    }

    private String name;
    private int value;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }
}
