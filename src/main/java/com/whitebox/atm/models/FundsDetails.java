package com.whitebox.atm.models;

import java.util.List;

public class FundsDetails {
    private List<BanknoteInventory> banknoteInventories;
    private int numberOfBanknoteTypes;

    public FundsDetails(List<BanknoteInventory> banknoteInventories, int numberOfBanknoteTypes) {
        this.banknoteInventories = banknoteInventories;
        this.numberOfBanknoteTypes = numberOfBanknoteTypes;
    }

    public List<BanknoteInventory> getBanknoteInventories() {
        return banknoteInventories;
    }

    public int getNumberOfBanknoteTypes() {
        return numberOfBanknoteTypes;
    }
}
