package com.whitebox.atm.models;

import java.util.*;

public class CashCounter {
    private int totalBanknotesType;
    private int[] minBanknotesPerAmount;
    private int[][] usedBanknotesTypePerAmount;

    public CashCounter(int amount, int totalBanknotesType) {
        this.totalBanknotesType = totalBanknotesType;
        minBanknotesPerAmount = new int[amount + 1];
        usedBanknotesTypePerAmount = new int[totalBanknotesType][amount + 1];
        Arrays.fill(minBanknotesPerAmount, amount + 1);
        minBanknotesPerAmount[0] = 0;
    }

    public int getMinBanknotesPerAmount(int amount) {
        return minBanknotesPerAmount[amount];
    }

    public int getUsedBanknotesTypePerAmount(int banknoteIndex, int amount) {
        return usedBanknotesTypePerAmount[banknoteIndex][amount];
    }

    public void setMinBanknotesPerAmount(int index, int value) {
        minBanknotesPerAmount[index] = value;
    }

    public void setUsedBanknotesTypePerAmount(int index, int amount, int value) {
        usedBanknotesTypePerAmount[index][amount] = value;
    }

    public int getTotalBanknotesType() {
        return totalBanknotesType;
    }
}
