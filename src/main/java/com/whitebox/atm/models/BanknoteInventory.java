package com.whitebox.atm.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Entity
public class BanknoteInventory {
    @Id
    private Banknote banknote;

    @Positive(message = "Quantity must be greater than 0.")
    @NotNull(message = "Quantity is required.")
    private Integer quantity;

    public BanknoteInventory() {

    }

    public BanknoteInventory(Banknote banknote, Integer quantity) {
        this.banknote = banknote;
        this.quantity = quantity;
    }

    public Banknote getBanknote() {
        return banknote;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "BanknoteInventory{" +
                "banknote=" + banknote +
                ", quantity=" + quantity +
                '}';
    }
}
