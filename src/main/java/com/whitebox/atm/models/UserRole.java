package com.whitebox.atm.models;

public enum UserRole {
    ADMIN("admin"),
    CLIENT("client");

    UserRole(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }
}
