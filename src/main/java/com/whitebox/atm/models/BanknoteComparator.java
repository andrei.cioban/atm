package com.whitebox.atm.models;

import java.util.Comparator;

public class BanknoteComparator implements Comparator<BanknoteInventory> {
    @Override
    public int compare(BanknoteInventory o1, BanknoteInventory o2) {
        return Integer.compare(o1.getBanknote().getValue(), o2.getBanknote().getValue());
    }
}
