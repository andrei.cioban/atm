package com.whitebox.atm.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table
public class Account {
    @Id
    private Integer id;

    @Column(unique = true)
    @NotBlank(message = "Iban is required.")
    private String iban;

    private int balance;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private CreditCard creditCard;

    public Account() {

    }

    public Account(String iban, int balance) {
        this.iban = iban;
        this.balance = balance;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @JsonBackReference
    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Account{" +
                "iban='" + iban + '\'' +
                ", balance=" + balance +
                '}';
    }
}