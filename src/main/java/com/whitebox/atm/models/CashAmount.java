package com.whitebox.atm.models;

import javax.validation.constraints.Min;

public class CashAmount {
    private Banknote banknote;

    @Min(value = 0, message = "Quantity must be positive.")
    private int quantity;

    public CashAmount(Banknote banknote, int quantity) {
        this.banknote = banknote;
        this.quantity = quantity;
    }

    public Banknote getBanknote() {
        return banknote;
    }

    public void setBanknote(Banknote banknote) {
        this.banknote = banknote;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CashAmount that = (CashAmount) o;
        return quantity == that.quantity && banknote == that.banknote;
    }

    @Override
    public String toString() {
        return "Banknote: " + banknote +
                ", quantity: " + quantity;
    }
}
