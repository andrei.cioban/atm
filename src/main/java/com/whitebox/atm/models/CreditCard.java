package com.whitebox.atm.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotBlank(message = "Card number is required.")
    private String cardNumber;

    private String bankName;
    private Integer cvv;

    @NotNull(message = "Pin is required.")
    private Integer pin;

    @OneToOne(mappedBy = "creditCard", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public CreditCard() {

    }

    public CreditCard(CreditCardBuilder builder) {
        this.cardNumber = builder.cardNumber;
        this.bankName = builder.bankName;
        this.cvv = builder.cvv;
        this.pin = builder.pin;
        this.account = builder.account;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public Integer getCvv() {
        return cvv;
    }

    public Integer getPin() {
        return pin;
    }

    public Integer getId() {
        return id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonBackReference
    public User getUser() {
        return user;
    }

    @JsonManagedReference
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void addAccount(Account account) {
        setAccount(account);
        account.setCreditCard(this);
    }

    public static class CreditCardBuilder {
        private Integer id;
        private String cardNumber;
        private String bankName;
        private Integer pin;
        private Integer cvv;
        private Account account;

        public CreditCardBuilder(String cardNumber, Integer pin, Account account) {
            this.cardNumber = cardNumber;
            this.pin = pin;
            this.account = account;
        }

        public CreditCardBuilder bankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        public CreditCardBuilder cvv(Integer cvv) {
            this.cvv = cvv;
            return this;
        }

        public CreditCard build() {
            return new CreditCard(this);
        }
    }
}