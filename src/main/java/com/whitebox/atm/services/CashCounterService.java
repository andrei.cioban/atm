package com.whitebox.atm.services;

import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteInventory;
import com.whitebox.atm.models.CashCounter;
import com.whitebox.atm.models.FundsDetails;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CashCounterService {
    private CashCounter cashCounter;

    public CashCounter initCashCounter(int amount, int totalBanknotesType) {
        return new CashCounter(amount, totalBanknotesType);
    }

    public void computeMinAmountBanknotes(int amount, FundsDetails fundsDetails) {
        cashCounter = initCashCounter(amount, fundsDetails.getNumberOfBanknoteTypes());
        List<BanknoteInventory> banknoteInventories = fundsDetails.getBanknoteInventories();

        for (int partialAmount = 1; partialAmount < amount + 1; ++partialAmount) {
            int currentAmount = partialAmount;

            banknoteInventories.forEach(inventory -> {
                Banknote banknote = inventory.getBanknote();
                int banknoteValue = banknote.getValue();
                int banknoteQuantity = inventory.getQuantity();
                int currentBanknotesPerAmount = cashCounter.getMinBanknotesPerAmount(currentAmount);
                int banknoteIndex = banknoteInventories.indexOf(inventory);

                updateMinNoBanknotes(currentAmount, banknoteValue, banknoteIndex, banknoteQuantity);
                if (currentBanknotesPerAmount != cashCounter.getMinBanknotesPerAmount(currentAmount)) {
                    updateUsedBanknotes(currentAmount, banknoteValue, banknoteIndex);
                }
            });
        }
    }

    public void updateMinNoBanknotes(int amount, int banknoteValue, int banknoteIndex, int banknoteQuantity) {
        if (banknoteValue <= amount &&
                cashCounter.getUsedBanknotesTypePerAmount(banknoteIndex, amount - banknoteValue) < banknoteQuantity) {
            int minNoBanknotes = Math.min(1 + cashCounter.getMinBanknotesPerAmount(amount - banknoteValue),
                    cashCounter.getMinBanknotesPerAmount(amount));
            cashCounter.setMinBanknotesPerAmount(amount, minNoBanknotes);
        }
    }

    public void updateUsedBanknotes(int amount, int banknoteValue, int banknoteIndex) {
        for (int i = 0; i < cashCounter.getTotalBanknotesType(); ++i) {
            int newValue = cashCounter.getUsedBanknotesTypePerAmount(i, amount - banknoteValue);
            cashCounter.setUsedBanknotesTypePerAmount(i, amount, newValue);
        }

        int value = cashCounter.getUsedBanknotesTypePerAmount(banknoteIndex, amount);
        cashCounter.setUsedBanknotesTypePerAmount(banknoteIndex, amount, value + 1);
    }

    public CashCounter getCashCounter() {
        return cashCounter;
    }
}
