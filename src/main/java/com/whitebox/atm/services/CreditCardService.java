package com.whitebox.atm.services;

import com.whitebox.atm.daos.CreditCardDao;
import com.whitebox.atm.models.Account;
import com.whitebox.atm.models.CreditCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditCardService {
    @Autowired
    CreditCardDao creditCardDao;

    public List<CreditCard> getCreditCards() {
        return creditCardDao.findAll();
    }

    public CreditCard getCreditCard(int id) {
        return creditCardDao.findById(id);
    }

    public CreditCard addCreditCard(CreditCard creditCard) {
        creditCardDao.save(creditCard);
        return creditCard;
    }

    public Account getAccountByCreditCardId(int creditCardId) {
        return creditCardDao.findAccount(creditCardId);
    }
}
