package com.whitebox.atm.services;

import com.whitebox.atm.exceptions.InvalidDataException;
import com.whitebox.atm.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WithdrawService {
    @Autowired
    private CashCounterService cashCounterService;

    @Autowired
    private BanknoteInventoryService banknoteInventoryService;

    public List<CashAmount> processWithdrawal(int amount, int balance) {
        if (balance < amount) {
            throw new InvalidDataException("Insufficient funds for withdrawal");
        }

        if (amount > banknoteInventoryService.getAmount()) {
            throw new InvalidDataException("Insufficient bank funds");
        }

        FundsDetails fundsDetails = initFundsDetails();
        cashCounterService.computeMinAmountBanknotes(amount, fundsDetails);

        return validateWithdrawal(amount, cashCounterService.getCashCounter());
    }

    public FundsDetails initFundsDetails() {
        return new FundsDetails(banknoteInventoryService.getBanknoteInventories(), banknoteInventoryService.getNumberOfBanknoteTypes());
    }

    public List<CashAmount> validateWithdrawal(int amount, CashCounter cashCounter) {
        if (cashCounter.getMinBanknotesPerAmount(amount) != amount + 1) {
            List<BanknoteInventory> banknoteInventories = banknoteInventoryService.getBanknoteInventories();
            List<CashAmount> cashAmounts = new ArrayList<>();

            banknoteInventories.forEach(cashEntry -> {
                Banknote banknote = cashEntry.getBanknote();
                int currentBanknoteQuantity = cashEntry.getQuantity();
                int banknoteIndex = banknoteInventories.indexOf(cashEntry);
                int usedBanknoteQuantity = cashCounter.getUsedBanknotesTypePerAmount(banknoteIndex, amount);

                banknoteInventoryService.modifyQuantity(banknote, currentBanknoteQuantity - usedBanknoteQuantity);
                cashAmounts.add(new CashAmount(banknote, usedBanknoteQuantity));
            });
            return cashAmounts;
        } else {
            return Collections.emptyList();
        }
    }
}
