package com.whitebox.atm.services;

import com.whitebox.atm.daos.BanknoteInventoryDao;
import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteComparator;
import com.whitebox.atm.models.BanknoteInventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BanknoteInventoryService {
    @Autowired
    private BanknoteInventoryDao banknoteInventoryDao;

    public List<BanknoteInventory> getBanknoteInventories() {
        List<BanknoteInventory> banknoteInventories = banknoteInventoryDao.findAll();
        banknoteInventories.sort(new BanknoteComparator());
        return banknoteInventories;
    }

    public BanknoteInventory addBanknoteInventory(BanknoteInventory banknoteInventory) {
        banknoteInventoryDao.save(banknoteInventory);
        return banknoteInventory;
    }

    public BanknoteInventory getBanknoteInventory(Banknote banknote) {
        return banknoteInventoryDao.findById(banknote);
    }

    public void modifyQuantity(Banknote banknote, int quantity) {
        banknoteInventoryDao.updateQuantity(banknote, quantity);
    }

    public int getAmount() {
        List<BanknoteInventory> banknoteInventories = getBanknoteInventories();
        return banknoteInventories.stream().map(banknoteInventory -> {
            int banknoteValue = banknoteInventory.getBanknote().getValue();
            int quantity = banknoteInventory.getQuantity();
            return banknoteValue * quantity;
        }).reduce(0, Integer::sum);
    }

    public List<BanknoteInventory> addBanknoteInventories(List<BanknoteInventory> banknoteInventories) {
        banknoteInventories.forEach(banknoteInventory -> banknoteInventoryDao.save(banknoteInventory));
        return banknoteInventories;
    }

    public int getNumberOfBanknoteTypes() {
        return getBanknoteInventories().size();
    }

    public List<BanknoteInventory> getModifiedBanknoteInventories(List<BanknoteInventory> banknoteInventories) {
        banknoteInventories.forEach(banknoteInventory ->
                modifyQuantity(banknoteInventory.getBanknote(), banknoteInventory.getQuantity()));

        return getBanknoteInventories();
    }

    public List<Banknote> getBanknotes() {
        return banknoteInventoryDao.findBanknotes();
    }
}
