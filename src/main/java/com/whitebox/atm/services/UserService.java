package com.whitebox.atm.services;

import com.whitebox.atm.daos.UserDao;
import com.whitebox.atm.models.CreditCard;
import com.whitebox.atm.models.User;
import com.whitebox.atm.models.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserDao userDao;

    public List<User> getUsers() {
        return userDao.findAll();
    }

    public User getUser(int id) {
        return userDao.findById(id);
    }

    public User getUser(String username, String password, UserRole userRole) {
        return userDao.findByCredentials(username, password, userRole);
    }

    public User addUser(User user) {
        userDao.save(user);
        return user;
    }

    public List<CreditCard> getCreditCardsByUserId(int userId) {
        return userDao.findCreditCards(userId);
    }

    public List<User> getUsersByRole(UserRole userRole) {
        return userDao.findByRole(userRole);
    }
}
