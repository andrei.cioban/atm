package com.whitebox.atm.services;

import com.whitebox.atm.daos.AccountDao;
import com.whitebox.atm.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {
    @Autowired
    AccountDao accountDao;

    public List<Account> getAccounts() {
        return accountDao.findAll();
    }

    public Account getAccount(int id) {
        return accountDao.findById(id);
    }

    public Account addAccount(Account account) {
        accountDao.save(account);
        return account;
    }

    public void modifyAccountBalance(int id, int newBalance) {
        accountDao.updateBalance(id, newBalance);
    }
}
