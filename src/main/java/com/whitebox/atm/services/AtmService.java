package com.whitebox.atm.services;

import com.whitebox.atm.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AtmService {
    @Autowired
    private WithdrawService withdrawService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private DepositService depositService;

    public int deposit(List<CashAmount> cashAmounts, int creditCardId) {
        Account account = creditCardService.getAccountByCreditCardId(creditCardId);
        int amount = depositService.processDeposit(cashAmounts);
        int newBalance = account.getBalance() + amount;
        accountService.modifyAccountBalance(account.getId(), newBalance);
        return amount;
    }

    public int getBalance(int creditCardId) {
        Account account = creditCardService.getAccountByCreditCardId(creditCardId);
        return account.getBalance();
    }

    public List<CashAmount> withdraw(int amount, int creditCardId) {
        Account account = creditCardService.getAccountByCreditCardId(creditCardId);

        List<CashAmount> cashAmounts = withdrawService.processWithdrawal(amount, account.getBalance());

        if (!cashAmounts.isEmpty()) {
            accountService.modifyAccountBalance(account.getId(), account.getBalance() - amount);
        }
        return cashAmounts;
    }
}
