package com.whitebox.atm.services;

import com.whitebox.atm.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepositService {

    @Autowired
    private BanknoteInventoryService banknoteInventoryService;

    public int processDeposit(List<CashAmount> cashAmounts) {
        storeCashIntoAtm(cashAmounts);
        return retrieveDepositedAmount(cashAmounts);
    }

    public void storeCashIntoAtm(List<CashAmount> cashAmounts) {
        cashAmounts.forEach(cashAmount -> {
            BanknoteInventory banknoteInventory = banknoteInventoryService.getBanknoteInventory(cashAmount.getBanknote());
            int currentQuantity = banknoteInventory.getQuantity();
            banknoteInventoryService.modifyQuantity(cashAmount.getBanknote(), currentQuantity + cashAmount.getQuantity());
        });
    }

    public int retrieveDepositedAmount(List<CashAmount> cashAmounts) {
        return cashAmounts.stream().map(cashAmount
                -> {
            Banknote banknote = cashAmount.getBanknote();
            int banknoteValue = banknote.getValue();
            int banknoteQuantity = cashAmount.getQuantity();
            return banknoteValue * banknoteQuantity;
        }).reduce(0, Integer::sum);
    }
}
