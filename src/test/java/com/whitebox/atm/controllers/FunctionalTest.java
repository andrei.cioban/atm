package com.whitebox.atm.controllers;

import com.whitebox.atm.models.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
public abstract class FunctionalTest {
    @LocalServerPort
    public int port;

    public List<BanknoteInventory> banknoteInventories;
    public List<CashAmount> cashAmounts;

    public User saveUser(UserRole userRole) {
        User user = new User();
        user.setName("Popescu Ion");
        user.setEmail("popion@gmail.com");
        user.setRole(userRole);
        user.setUsername("Nutu");
        user.setPassword("1234");
        user.setCreditCards(null);

        RestAssured
                .given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users");

        return user;
    }

    public void saveUserFullInfos(int balance, UserRole userRole) {
        User user = new User();
        user.setName("Popescu Ion");
        user.setEmail("popion@gmail.com");
        user.setRole(userRole);
        user.setUsername("Nutu");
        user.setPassword("1234");
        user.setCreditCards(List.of(
                new CreditCard.CreditCardBuilder("5105 1051 0510 5100", 7200,
                        new Account("RO49 ABBA 1B31 0075 9384 0000", balance)).build()
        ));

        RestAssured
                .given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users");
    }

    public List<BanknoteInventory> putBanknoteInventories() {
        setList(new int[]{100, 100, 100, 100, 100, 100}, banknoteInventories);

        RestAssured
                .given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(banknoteInventories)
                .when()
                .put("/atm/funds");

        return banknoteInventories;
    }

    public void postBanknoteInventories(List<BanknoteInventory> banknoteInventories) {
        RestAssured
                .given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(banknoteInventories)
                .when()
                .post("/atm/funds");
    }

    public boolean areFundsEmpty() {
        Response response = RestAssured
                .given()
                .port(port)
                .when()
                .get("/atm/funds")
                .then()
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.asString());
        return !jsonPath.get("size()").equals(banknoteInventories.size());
    }

    public boolean isClientAdded(int clientId) {
        Response response = RestAssured
                .given()
                .port(port)
                .when()
                .get("/users/" + clientId)
                .then()
                .extract()
                .response();

        return !response.getBody().asString().equals("User not found.");
    }

    public void initLists() {
        banknoteInventories = Arrays.asList(
                new BanknoteInventory(Banknote.ONE_RON, 0),
                new BanknoteInventory(Banknote.FIVE_RON, 0),
                new BanknoteInventory(Banknote.TEN_RON, 0),
                new BanknoteInventory(Banknote.TWENTY_RON, 0),
                new BanknoteInventory(Banknote.FIFTY_RON, 0),
                new BanknoteInventory(Banknote.ONE_HUNDRED_RON, 0)
        );

        cashAmounts = Arrays.asList(
                new CashAmount(Banknote.ONE_RON, 0),
                new CashAmount(Banknote.FIVE_RON, 0),
                new CashAmount(Banknote.TEN_RON, 0),
                new CashAmount(Banknote.TWENTY_RON, 0),
                new CashAmount(Banknote.FIFTY_RON, 0),
                new CashAmount(Banknote.ONE_HUNDRED_RON, 0)
        );
    }

    public <T> void setList(int[] quantities, List<T> list) {
        for (int i = 0; i < quantities.length; ++i) {
            if (list.get(0) instanceof BanknoteInventory) {
                banknoteInventories.set(i, new BanknoteInventory(banknoteInventories.get(i).getBanknote(), quantities[i]));
            } else if (list.get(0) instanceof CashAmount) {
                cashAmounts.set(i, new CashAmount(banknoteInventories.get(i).getBanknote(), quantities[i]));
            }
        }
    }
}
