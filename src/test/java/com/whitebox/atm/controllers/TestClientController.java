package com.whitebox.atm.controllers;

import com.whitebox.atm.models.User;
import com.whitebox.atm.models.UserRole;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;;
import org.junit.jupiter.api.Test;

public class TestClientController extends FunctionalTest {
    @Test
    public void given_client_when_save_then_return_success() {
        User user = new User();
        user.setName("Popescu Ionut");
        user.setEmail("popion2@gmail.com");
        user.setRole(UserRole.CLIENT);
        user.setUsername("Nutu#1");
        user.setPassword("1234");
        user.setCreditCards(null);

        RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users")
                .then()
                .statusCode(200);
    }

    @Test
    public void when_get_clients_then_return_success() {
        saveUser(UserRole.CLIENT);

        RestAssured
                .given()
                .port(super.port)
                .when()
                .get("/users")
                .then()
                .statusCode(200);
    }

    @Test
    public void when_get_client_with_valid_id_then_return_success() {
        saveUser(UserRole.CLIENT);

        RestAssured
                .given()
                .port(super.port)
                .when()
                .get("/users/1")
                .then()
                .statusCode(200);
    }

    @Test
    public void when_get_client_with_invalid_id_then_return_failure() {
        saveUser(UserRole.CLIENT);

        RestAssured
                .given()
                .port(super.port)
                .when()
                .get("/users/-1")
                .then()
                .statusCode(500);
    }

    @Test
    public void when_get_not_found_client_then_return_failure() {
        saveUser(UserRole.CLIENT);

        RestAssured
                .given()
                .port(super.port)
                .when()
                .get("/users/120")
                .then()
                .statusCode(404);
    }

    @Test
    public void given_client_with_missing_fields_when_save_then_return_failure() {
        User client = saveUser(UserRole.CLIENT);
        client.setName(null);
        client.setEmail(" ");

        RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .when()
                .post("/users")
                .then()
                .statusCode(400);
    }
}
