package com.whitebox.atm.controllers;

import com.whitebox.atm.models.CashAmount;
import com.whitebox.atm.models.UserRole;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestAtmController extends FunctionalTest {
    @BeforeEach
    public void setup() {
        initLists();
    }

    @Test
    public void given_funds_when_save_then_return_success() {
        if (areFundsEmpty()) {
            setList(new int[]{5, 5, 5, 5, 5, 5}, banknoteInventories);

            RestAssured
                    .given()
                    .port(super.port)
                    .contentType(ContentType.JSON)
                    .body(banknoteInventories)
                    .when()
                    .post("/atm/funds")
                    .then()
                    .statusCode(200)
                    .body("", Matchers.hasSize(6));
        }
    }

    @Test
    public void given_funds_when_put_then_return_success() {
        if (areFundsEmpty()) {
            setList(new int[]{100, 100, 100, 100, 100, 100}, banknoteInventories);
            postBanknoteInventories(banknoteInventories);
        }

        setList(new int[]{5, 5, 5, 5, 5, 5}, banknoteInventories);
        Response response = RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(banknoteInventories)
                .when()
                .put("/atm/funds")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.asString());
        Assertions.assertEquals("5", jsonPath.getString("[0].quantity"));
    }

    @Test
    public void when_get_funds_then_return_success() {
        if (areFundsEmpty()) {
            setList(new int[]{100, 100, 100, 100, 100, 100}, banknoteInventories);
            postBanknoteInventories(banknoteInventories);
        }

        RestAssured
                .given()
                .port(super.port)
                .when()
                .get("/atm/funds")
                .then()
                .statusCode(200)
                .body("", Matchers.hasSize(6));
    }

    @Test
    public void given_credit_card_negative_id_when_get_balance_then_return_failure() {
        RestAssured
                .given()
                .port(super.port)
                .queryParam("creditCardId", -1)
                .when()
                .get("/atm/balance")
                .then()
                .statusCode(500);
    }

    @Test
    public void given_credit_card_not_found_id_when_get_balance_then_return_failure() {
        RestAssured
                .given()
                .port(super.port)
                .queryParam("creditCardId", Integer.MAX_VALUE)
                .when()
                .get("/atm/balance")
                .then()
                .statusCode(404);
    }

    @Test
    public void given_credit_card_valid_id_when_get_balance_then_return_success() {
        if (!isClientAdded(2)) {
            saveUserFullInfos(2500, UserRole.CLIENT);
        }

        Response response = RestAssured
                .given()
                .port(super.port)
                .queryParam("creditCardId", 2)
                .when()
                .get("/atm/balance")
                .then()
                .statusCode(200)
                .extract()
                .response();

        Assertions.assertEquals("2500", response.getBody().asString());
    }

    @Test
    public void given_valid_cash_amount_and_credit_card_id_when_deposit_then_return_success() {
        if (areFundsEmpty()) {
            setList(new int[]{100, 100, 100, 100, 100, 100}, banknoteInventories);
            postBanknoteInventories(banknoteInventories);
        }

        if (!isClientAdded(2)) {
            saveUserFullInfos(2500, UserRole.CLIENT);
        }

        setList(new int[]{0, 0, 0, 0, 1, 7}, cashAmounts);

        Response response = RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(cashAmounts)
                .queryParam("creditCardId", 2)
                .when()
                .post("/atm/deposit")
                .then()
                .statusCode(200)
                .extract()
                .response();

        Assertions.assertEquals("750", response.getBody().asString());
    }

    @Test
    public void given_valid_credit_card_id_and_amount_when_withdraw_then_return_success() {
        if (areFundsEmpty()) {
            setList(new int[]{100, 100, 100, 100, 100, 100}, banknoteInventories);
            postBanknoteInventories(banknoteInventories);
        }

        if (!isClientAdded(2)) {
            saveUserFullInfos(2500, UserRole.CLIENT);
        }

        int amount = 428;
        Response response = RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .queryParam("creditCardId", 2)
                .queryParam("amount", amount)
                .when()
                .get("/atm/withdraw")
                .then()
                .statusCode(200)
                .extract()
                .response();

        List<CashAmount> result = response.jsonPath().getList("", CashAmount.class);
        setList(new int[]{3, 1, 0, 1, 0, 4}, cashAmounts);
        cashAmounts.forEach(cashAmount ->
                Assertions.assertEquals(cashAmount, result.get(result.indexOf(cashAmount))));
    }

    @Test
    public void given_amount_more_than_balance_when_withdraw_then_return_failure() {
        if (!isClientAdded(2)) {
            saveUserFullInfos(2500, UserRole.CLIENT);
        }

        int amount = 5200;
        RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .queryParam("creditCardId", 2)
                .queryParam("amount", amount)
                .when()
                .get("/atm/withdraw")
                .then()
                .statusCode(400);
    }

    @Test
    public void given_amount_more_than_atm_funds_when_withdraw_then_return_failure() {
        if (areFundsEmpty()) {
            setList(new int[]{100, 20, 0, 0, 0, 8}, banknoteInventories);
            postBanknoteInventories(banknoteInventories);
        }

        if (!isClientAdded(2)) {
            saveUserFullInfos(2500, UserRole.CLIENT);
        }
        int amount = 1300;

        RestAssured
                .given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .queryParam("creditCardId", 2)
                .queryParam("amount", amount)
                .when()
                .get("/atm/withdraw")
                .then()
                .statusCode(400);
    }
}
