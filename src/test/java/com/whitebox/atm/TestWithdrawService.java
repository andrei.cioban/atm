package com.whitebox.atm;

import com.whitebox.atm.exceptions.InvalidDataException;
import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteInventory;
import com.whitebox.atm.models.CashAmount;
import com.whitebox.atm.services.BanknoteInventoryService;
import com.whitebox.atm.services.CashCounterService;
import com.whitebox.atm.services.WithdrawService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestWithdrawService {
    @InjectMocks
    private WithdrawService withdrawService;

    @Mock
    private BanknoteInventoryService banknoteInventoryService;

    @Spy
    private CashCounterService cashCounterService;

    private List<BanknoteInventory> banknoteInventories;
    private List<CashAmount> cashAmounts;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        initLists();
    }

    public void initLists() {
        banknoteInventories = Arrays.asList(
                new BanknoteInventory(Banknote.ONE_RON, 0),
                new BanknoteInventory(Banknote.FIVE_RON, 0),
                new BanknoteInventory(Banknote.TEN_RON, 0),
                new BanknoteInventory(Banknote.TWENTY_RON, 0),
                new BanknoteInventory(Banknote.FIFTY_RON, 0),
                new BanknoteInventory(Banknote.ONE_HUNDRED_RON, 0)
        );

        cashAmounts = Arrays.asList(
                new CashAmount(Banknote.ONE_RON, 0),
                new CashAmount(Banknote.FIVE_RON, 0),
                new CashAmount(Banknote.TEN_RON, 0),
                new CashAmount(Banknote.TWENTY_RON, 0),
                new CashAmount(Banknote.FIFTY_RON, 0),
                new CashAmount(Banknote.ONE_HUNDRED_RON, 0)
        );
    }

    public <T> void setList(int[] quantities, List<T> list) {
        for (int i = 0; i < quantities.length; ++i) {
            if (list.get(0) instanceof BanknoteInventory) {
                banknoteInventories.set(i, new BanknoteInventory(banknoteInventories.get(i).getBanknote(), quantities[i]));
            } else if (list.get(0) instanceof CashAmount) {
                cashAmounts.set(i, new CashAmount(banknoteInventories.get(i).getBanknote(), quantities[i]));
            }
        }
    }

    @Test
    public void given_insufficient_account_funds_when_withdraw_then_throw_exception() {
        Assertions.assertThrows(InvalidDataException.class, () ->
                withdrawService.processWithdrawal(2550, 2500));
    }

    @Test
    public void given_insufficient_atm_funds_when_withdraw_then_throw_exception() {
        Mockito.when(banknoteInventoryService.getAmount()).thenReturn(1200);

        Assertions.assertThrows(InvalidDataException.class, () ->
                withdrawService.processWithdrawal(1300, 2500));
    }

    @Test
    public void given_sufficient_funds_when_withdraw_then_result_is_correct() {
        setList(new int[]{100, 100, 100, 100, 100, 100}, banknoteInventories);

        Mockito.when(banknoteInventoryService.getAmount()).thenReturn(5000);
        Mockito.when(banknoteInventoryService.getBanknoteInventories()).thenReturn(banknoteInventories);
        Mockito.when(banknoteInventoryService.getNumberOfBanknoteTypes()).thenReturn(banknoteInventories.size());

        List<CashAmount> result = withdrawService.processWithdrawal(679, 3000);
        setList(new int[]{4, 1, 0, 1, 1, 6}, cashAmounts);
        Assertions.assertEquals(cashAmounts, result);
    }

    @Test
    public void given_specific_atm_funds_when_withdraw_then_withdrawal_is_empty() {
        setList(new int[]{10, 0, 0, 1, 1, 100}, banknoteInventories);
        Mockito.when(banknoteInventoryService.getAmount()).thenReturn(1080);
        Mockito.when(banknoteInventoryService.getBanknoteInventories()).thenReturn(banknoteInventories);
        Mockito.when(banknoteInventoryService.getNumberOfBanknoteTypes()).thenReturn(banknoteInventories.size());

        List<CashAmount> result = withdrawService.processWithdrawal(195, 3000);
        Assertions.assertEquals(Collections.emptyList(), result);
    }
}
