package com.whitebox.atm;

import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteInventory;
import com.whitebox.atm.models.CashCounter;
import com.whitebox.atm.models.FundsDetails;
import com.whitebox.atm.services.CashCounterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;

public class TestCashCounterService {
    @Spy
    private CashCounterService spyCashCounterService;

    @Mock
    private FundsDetails fundsDetails;

    private CashCounter cashCounter;
    private List<BanknoteInventory> banknoteInventories;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        initBanknoteInventories();
        Mockito.when(fundsDetails.getBanknoteInventories()).thenReturn(banknoteInventories);
    }

    public void initBanknoteInventories() {
        banknoteInventories = Arrays.asList(
                new BanknoteInventory(Banknote.ONE_RON, 0),
                new BanknoteInventory(Banknote.FIVE_RON, 0),
                new BanknoteInventory(Banknote.TEN_RON, 0),
                new BanknoteInventory(Banknote.TWENTY_RON, 0),
                new BanknoteInventory(Banknote.FIFTY_RON, 0),
                new BanknoteInventory(Banknote.ONE_HUNDRED_RON, 0)
        );
    }

    public void setBanknoteInventories(int[] quantities) {
        for (int i = 0; i < quantities.length; ++i) {
            banknoteInventories.set(i, new BanknoteInventory(banknoteInventories.get(i).getBanknote(), quantities[i]));
        }
    }

    @Test
    public void given_insufficient_bank_funds_when_attempt_to_get_min_banknotes_then_number_of_banknotes_is_incorrect() {
        setBanknoteInventories(new int[]{4, 2, 3, 4, 0, 0});
        cashCounter = new CashCounter(162, banknoteInventories.size());

        Mockito.when(spyCashCounterService.initCashCounter(anyInt(), anyInt())).thenReturn(cashCounter);
        spyCashCounterService.computeMinAmountBanknotes(162, fundsDetails);

        Assertions.assertEquals(163, spyCashCounterService.getCashCounter().getMinBanknotesPerAmount(162));
    }

    @Test
    public void given_sufficient_bank_funds_when_attempt_to_get_min_banknotes_then_number_of_banknotes_is_correct() {
        setBanknoteInventories(new int[]{200, 200, 200, 200, 200, 200});
        cashCounter = new CashCounter(332, banknoteInventories.size());

        Mockito.when(spyCashCounterService.initCashCounter(anyInt(), anyInt())).thenReturn(cashCounter);
        spyCashCounterService.computeMinAmountBanknotes(332, fundsDetails);

        Assertions.assertEquals(7, spyCashCounterService.getCashCounter().getMinBanknotesPerAmount(332));
    }

    @Test
    public void given_specific_bank_funds_when_attempt_to_get_min_banknotes_then_number_of_banknotes_for_greedy_is_incorrect() {
        setBanknoteInventories(new int[]{20, 0, 0, 5, 1, 3});
        cashCounter = new CashCounter(161, banknoteInventories.size());
        int greedyResult = 13;

        Mockito.when(spyCashCounterService.initCashCounter(anyInt(), anyInt())).thenReturn(cashCounter);
        spyCashCounterService.computeMinAmountBanknotes(161, fundsDetails);

        Assertions.assertNotEquals(greedyResult, cashCounter.getMinBanknotesPerAmount(161));
    }

    @Test
    public void given_sufficient_bank_funds_when_attempt_to_get_min_banknotes_then_used_banknotes_of_each_type_are_correct() {
        cashCounter = new CashCounter(689, banknoteInventories.size());
        setBanknoteInventories(new int[]{200, 200, 200, 200, 200, 200});

        Mockito.when(spyCashCounterService.initCashCounter(anyInt(), anyInt())).thenReturn(cashCounter);
        spyCashCounterService.computeMinAmountBanknotes(689, fundsDetails);

        int[] expected = new int[]{4, 1, 1, 1, 1, 6};
        for (int i = 0; i < banknoteInventories.size(); ++i) {
            Assertions.assertEquals(expected[i], spyCashCounterService.getCashCounter()
                    .getUsedBanknotesTypePerAmount(i, 689));
        }
    }
}
